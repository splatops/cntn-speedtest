    # ############################################### #
    # #  splatops/cntn-speedtest/README ############ #
    # ############################################# #
    #
    # Brought to you by...
    # 
    # ::::::::::::'#######::'########:::'######::
    # :'##::'##::'##.... ##: ##.... ##:'##... ##:
    # :. ##'##::: ##:::: ##: ##:::: ##: ##:::..::
    # '#########: ##:::: ##: ########::. ######::
    # .. ## ##.:: ##:::: ##: ##.....::::..... ##:
    # : ##:. ##:: ##:::: ##: ##::::::::'##::: ##:
    # :..:::..:::. #######:: ##::::::::. ######::
    # ::::::::::::.......:::..::::::::::......:::
    #
    ##################################################
   

# Containerized Speedtest
SplatOps presents an open source contribution to aid end users of Ookla Speedtest CLI who are looking a bundled solution to collect and analyze the data in an automated fashion.

Notable software used in this project includes:

* [Speedtest CLI](https://www.speedtest.net/apps/cli)
* [Grafana](https://grafana.com/)
* [InfluxDB](https://www.influxdata.com/products/influxdb-overview/)

**Note:** *This project is not in any way affiliated with any of the organizations referenced.  Since this project uses tools that leverage online services we want to let you know that those services may collect your data, however we do not.  Please verify any terms of service and license agreements prior to running this software.*

This project provisions an environment that includes Speedtest (CLI variant), Grafana and InfluxDB.  That looks like this:

![SplatOps Internet Monitoring Dashboard](/assets/cntn-speedtest.png "In all it's glory..")

## Getting Started
These instructions will walk you through:

* Building the Speedtest Stack Using `docker-compose`
* Running the Speedtest Stack After Build
* Customizing the Speedtest Stack *(TBD)*
* Removing the SplatOps Speedtest Stack *(TBD)*
* Running the Speedtest Container Standalone *(TBD)*

Note: This guide assumes Docker is installed and generally understood by the end user.

### Building the Speedtest Stack Using `docker-compose`
Clone this repository. Then...
* ```cd cntn-speedtest```
* ```docker-compose build```

That's it.  The ```docker-compose``` will build three container images:
* cntn-speedtest_influxdb
* cntn-speedtest_grafana
* cntn-speedtest_speedtest

You can validate this has been done by issuing ```docker image ls```.

### Running the Speedtest Stack After Build
**IMPORTANT INFORMATION:** *The Speedtest CLI application is a product of Ookla.  To use the tool the end user must accept an end user agreement.  This project does not attempt to automate accepting this agreement for you.  You will have to interactively accept the agreement before this tool will start collecting metrics for you!*

The Speedtest stack was designed to "just work" with no configuration required.  Customization, however, was added such that most changes only require minor environment overrides and template changes.

To start the Speedtest stack on a first run two steps need to be completed.  The first is that the stack is started using ```docker-compose```.  The second is that the end user agreement needs to be accepted in the ```speedtest``` container.

To get the stack running:
* Validate you are in the root of the ```cntn-speedtest``` directory.
* Run the following command:
  * ```# docker-compose up -d```
  * This will run the stack detached and fall back to your CLI with no output.
* Validate the containers are running:
  * ```# docker container ls | grep cntn```
  * Three containers should be running:
    * ```speedtest```
    * ```grafana```
    * ```influxdb```
* As per the note at the beginning of this section it is required to accept the end user agreement from Ookla in the ```speedtest``` container.  To do that exec into the running ```speedtest``` container and run the tool manually as follows:
  * ```# docker container exec -it speedtest bash```
* Once in the container run the Speedtest CLI tool (not the Python application) as follows:
  * ```# speedtest```
  * Agree to the license by typing ```yes```.
  * At this point ```speedtest``` will start. You can either let tspeedtest run (the values will not be saved) or cancel it as the license file has been generated and saved within the Docker named volume for the ```speedtest``` container.
  * Exit the container by typing: ```exit```.
* Everything should be working approriately now.  The default timing to run the speedtest check is every 30 minutes.  You will not see plotted data until then unless you have overridden the default speedtest timeframe via the ```ST_SLEEP``` environment variable.
* Log in to Grafana at the following URL:
  * ```http://<IP_of_Docker_Node>:3000```
  * Default username is unchanged for Grafana (```admin```:```admin```).
* Click the ```Home``` drop-down and select to view the ```SplatOps Internet Monitoring``` dashboard.

### Coming Soon...
* Customizing the Speedtest Stack
* Removing the SplatOps Speedtest Stack
* Running the Speedtest Container Standalone
* FAQ

