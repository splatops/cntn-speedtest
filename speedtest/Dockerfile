#
# ################################################### #
# #  splatops/cntn-speedtest/speedtest/Dockerfile ## #
# ################################################# #
#
# Brought to you by...
# 
# ::::::::::::'#######::'########:::'######::
# :'##::'##::'##.... ##: ##.... ##:'##... ##:
# :. ##'##::: ##:::: ##: ##:::: ##: ##:::..::
# '#########: ##:::: ##: ########::. ######::
# .. ## ##.:: ##:::: ##: ##.....::::..... ##:
# : ##:. ##:: ##:::: ##: ##::::::::'##::: ##:
# :..:::..:::. #######:: ##::::::::. ######::
# ::::::::::::.......:::..::::::::::......:::
#
##################################################
# CONTAINER BASE
FROM python:3.8-buster

# INITIAL CONFIGURABLE VARIABLES
ENV \
    # SPEEDTEST CONTAINER USER
    ST_USER=speedtest \
    # SPEEDTEST REPO KEY
    ST_KEY=379CE192D401AB61 \
    # MAIN PATH
    ST_BIN=/speedtest \
    # TIME IN SECONDS FOR EXECUTION
    # e.g. ST_SLEEP=1800
    ST_SLEEP=1800 \
    #
    # CONFIGURABLE ENV FOR SPEEDTEST.PY
    #
    # NAME OF INFLUXDB CONTAINER
    ST_INFLUXDB_HOST='influxdb' \
    # PORT OF INFLUXDB CONTAINER
    ST_INFLUXDB_PORT=8086 \
    # INFLUXDB DB NAME
    ST_INFLUXDB_NAME='speedtest' \
    # INFLUXDB MEASUREMENT "CONTAINER" (NOT DOCKER)
    ST_INFLUXDB_MEASUREMENT='speedtest' \
    # INFLUXDB ID TAG FOR MEASUREMENT "CONTAINER" (NOT DOCKER)
    ST_INFLUXDB_TAG_ID='splatops' \
    # SPEEDTEST CLI FLAGS
    ST_FLAGS='-f json' \
    # SPEEDTEST CONTAINER LOG LOCATION AND NAME
    ST_LOG_NAME='var/log/speedtest.log'

# VARIABLES TO SET AFTER INITIALS
ENV \
    # SPEEDTEST LICENSE PATH
    ST_LICENSE=/home/{ST_USER}/.config/ookla \
    # SET FULL PATH
    PATH=${PATH}:${ST_BIN}

# ENVIRONMENT BUILD
RUN \
echo "### Install dependencies ###" && \
apt update && \
apt install -y apt-transport-https apt-utils dirmngr gnupg1 lsb-release vim && \
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys ${ST_KEY} && \
echo "deb https://ookla.bintray.com/debian $(lsb_release -sc) main" | tee  /etc/apt/sources.list.d/speedtest.list && \
apt update && \
apt upgrade -y && \
echo "### Install Speedtest after provisioning ###" && \
apt install -y speedtest && \
echo "### Install Python dependencies ###" && \
pip install influxdb ptpython && \
echo "### Clean up ###" && \
apt clean && \
rm -rf /var/lib/apt/lists/*

# ADD RUNTIME USER & WORK ENV


# CONTAINER STAGING
WORKDIR ${ST_BIN}
COPY speedtest.py .
COPY entrypoint.sh .
RUN \
echo "### Add non-root user ###" && \
useradd -ms /bin/bash ${ST_USER} && \
echo "### Setup folder structure ###" && \
bash -c 'mkdir -p ${ST_BIN}/var/{log,opt}' && \
chown -R ${ST_USER}:${ST_USER} ${ST_BIN} && \
chmod +x speedtest.py entrypoint.sh

# SELECT RUNTIME USER
USER ${ST_USER}

ENTRYPOINT [ "entrypoint.sh" ]
CMD [ "speedtest.py" ]