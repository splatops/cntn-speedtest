#!/usr/bin/env python
#
# ##################################################### #
# #  splatops/cntn-speedtest/speedtest/speedtest.py ## #
# ################################################### #
#
# Brought to you by...
# 
# ::::::::::::'#######::'########:::'######::
# :'##::'##::'##.... ##: ##.... ##:'##... ##:
# :. ##'##::: ##:::: ##: ##:::: ##: ##:::..::
# '#########: ##:::: ##: ########::. ######::
# .. ## ##.:: ##:::: ##: ##.....::::..... ##:
# : ##:. ##:: ##:::: ##: ##::::::::'##::: ##:
# :..:::..:::. #######:: ##::::::::. ######::
# ::::::::::::.......:::..::::::::::......:::
#
##################################################
from influxdb import InfluxDBClient
import json
import logging
import logging.handlers as handlers
import os
import subprocess
import sys
import time


# USER CONFIGURABLE GLOBALS
INFLUXDB_HOST = os.environ.get('ST_INFLUXDB_HOST')
INFLUXDB_PORT = os.environ.get('ST_INFLUXDB_PORT')
INFLUXDB_NAME = os.environ.get('ST_INFLUXDB_NAME')
INFLUXDB_MEASUREMENT = os.environ.get('ST_INFLUXDB_MEASUREMENT')
INFLUXDB_TAG_ID = os.environ.get('ST_INFLUXDB_TAG_ID')
FLAGS = os.environ.get('ST_FLAGS')
LOG_NAME = os.environ.get('ST_LOG_NAME')


# Setup logging
local_logger = logging.getLogger()
local_logger.setLevel(logging.DEBUG)
log_formatter = logging.Formatter('%(asctime)s-%(levelname)s | %(message)s', datefmt='%Y%m%d-%H:%M:%S')
#
file_handler = logging.handlers.RotatingFileHandler(LOG_NAME, maxBytes=500000, backupCount=1)
file_handler.setFormatter(log_formatter)
local_logger.addHandler(file_handler)
#
console_handler = logging.StreamHandler()
console_handler.setFormatter(log_formatter)
local_logger.addHandler(console_handler)


# Setup InfluxDB Client
try:
    db_client = InfluxDBClient(host=INFLUXDB_HOST, port=INFLUXDB_PORT)
    logging.info(f"InfluxDB client connection created: {INFLUXDB_HOST} @ {INFLUXDB_PORT}")
except:
    logging.error(f"InfluxDB client connection failed, exiting.")
    sys.exit()

# Check for required environment variables
if os.environ.get('ST_SLEEP') == '':
    logging.warning("ST_SLEEP environment variable not set, exiting")
    sys.exit()
else:
    SLEEP_SECS = int(os.environ.get('ST_SLEEP'))
    logging.info(f"ST_SLEEP environment variable set to: {SLEEP_SECS}")


def to_mbits(st_bytes):
    return float(st_bytes)/125000


def speedtest_runner(flags):
    try:
        # Run subprocess
        completed = subprocess.run(
            f'speedtest {flags}',
            check = True,
            shell = True,
            stdout = subprocess.PIPE,
            stderr = subprocess.PIPE
        )
    except subprocess.CalledProcessError as e:
        # Generate log - subprocess value exception
        logging.warning(f'ValueError: {e}')
        return 0
    else:
        # Generate log - measurement is functioning
        try:
            logging.info(f'Command run successfully: ({completed.args})')
            jblob = json.loads(completed.stdout.decode("utf-8"))
            mbits_dn = to_mbits(jblob["download"]["bandwidth"])
            mbits_up = to_mbits(jblob["upload"]["bandwidth"])
            logging.info(f'timestamp:({jblob["timestamp"]})')
            logging.info(f'latency:({jblob["ping"]["latency"]}),jitter:({jblob["ping"]["jitter"]})')
            logging.info(f'id:({jblob["server"]["id"]}),name:({jblob["server"]["name"]}),location:({jblob["server"]["location"]}),country:({jblob["server"]["country"]})')
            logging.info(f'down:({mbits_dn}),up:({mbits_up})')

            # Debug to dump full JSON blob
            # logging.info(f'{jblob}')


            # Template to write to database
            influx_template = [
                {
                    "measurement": f"{INFLUXDB_MEASUREMENT}-{INFLUXDB_TAG_ID}",
                    "tags": {
                        "id": INFLUXDB_TAG_ID
                    },
                    "time": jblob["timestamp"],
                    "fields": {
                        "latency": float(jblob["ping"]["latency"]),
                        "jitter": float(jblob["ping"]["jitter"]),
                        "download": float(mbits_dn),
                        "upload": float(mbits_up),
                        "server_id": jblob["server"]["id"],
                        "server_name": jblob["server"]["name"],
                        "server_location": jblob["server"]["location"],
                        "server_country": jblob["server"]["country"]
                    }
                }
            ]

            # Debug to dump InfluxDB template
            # logging.info(f'{influx_template}')

            db_client.write_points(influx_template)
            logging.info(f'Database insert success ({INFLUXDB_NAME}).')
        except Exception as e:
            logging.error(f'Unknown error: ({e}).')
        return 1


def pre_flight():
    pre_flight_counter = 0
    db_found = False
    try:
        for db in db_client.get_list_database():
            pre_flight_counter += 1
            db_len = len(db_client.get_list_database())
            if db['name'] == INFLUXDB_NAME:
                logging.info(f'Database {INFLUXDB_NAME} exists, skipping creation ({pre_flight_counter} of {db_len}).')
                db_found = True
            elif db_found == False:
                logging.info(f'Database found but not a match ({pre_flight_counter} of {db_len}).')
        if db_found == False:
            logging.info(f'Database {INFLUXDB_NAME} not found, creating.')
            db_client.create_database(INFLUXDB_NAME)
        logging.info(f'Setting database ({INFLUXDB_NAME}).')
    except:
        logging.info(f'Database configuration or connectivity error, exiting.')
        sys.exit()
    try:
        db_client.switch_database(INFLUXDB_NAME)
        logging.info(f'Database successfully set ({INFLUXDB_NAME}).')
    except:
        logging.info(f'Database set failure, exiting.')
        sys.exit()


def main():
    while True:
        speedtest_status = 0
        logging.info("### Starting Speedtest CLI")
        speedtest_status = speedtest_runner(FLAGS)
        if speedtest_status == 1:
            logging.info("### Ending Speedtest CLI - Success")
            pass
        else:
            logging.info("### Ending Speedtest CLI - Failure")
        time.sleep(SLEEP_SECS)


if __name__ == '__main__':
    pre_flight()
    main()